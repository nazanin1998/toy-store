
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/DataServe/product-data-serve.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Utills/file-utills.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/Product/product-single.dart';
import 'package:toy_store/main.dart';
import 'package:toy_store/views/Product/product-horizontal-list.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab>{
  int _currentSlider = 0;
  bool loadingImage = true;

  List<Product> slideImgList = [];

  List<File> files = [];

  _HomeTabState(){
    slideImgList = AppState.instance.getNewestProducts();
    _loadCaroselImages();
  }

  _loadCaroselImages()async{
    for(var i=0; i < slideImgList.length ; i++){
       File file = await FileUtils.loadBase(slideImgList[i].images[0], slideImgList[i].id+ new DateTime.now().toString().replaceAll(" ", "-")+"0.png");
       files.add(file);
    }
    setState(() {
      loadingImage = false;
    });

  }
  @override
  Widget build(BuildContext context) {

    double width =  MediaQuery.of(context).size.width;
    double height =  MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,

        body:NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  automaticallyImplyLeading: false,

                    backgroundColor: Theme.of(context).backgroundColor,
                    expandedHeight: height/6,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      titlePadding:  EdgeInsets.only(right: 20, bottom: 20),
                      title: Text("خانه",
                          style: TextStyle(
                              color: Theme.of(context).textSelectionColor,
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold
                          )),
                      background: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            width: 40,
                            height: 4,
                            color: Theme.of(context).accentColor,
                          )
                        ],
                      ),
                    )
                ),
              ];
            },
            body:SingleChildScrollView(

              child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _titleRow(StringUtils.NEWEST_TXT,height, width),
                      getCarousel(height, width),
                      _titleRow(StringUtils.BEST_SELLER,height, width),
                      ProductDataServer(params: "bestseller",isVertical: false, ),
                      _titleRow(StringUtils.MOST_VIEWER,height, width),
                      ProductDataServer(params: "",isVertical: false, ),
                    ],

              ),
            )
        ));
  }

  Widget _titleRow(String title, double height, double width) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20, top: 20, right: 20, left: 20),
      child: Text(title, style: TextStyle(color:Theme.of(context).primaryColor, fontWeight: FontWeight.bold, fontSize: 15,),),
    );
  }

  getFileIndex(product){
    for(var i = 0 ; i < slideImgList.length ; i++){
      if(product.id == slideImgList[i].id)
        return i;
    }
    return 0;
  }
  Widget getCarousel(double height, double width) {

    return  Container(
      color: Colors.transparent,
      child: Column(
        children: <Widget>[
          CarouselSlider(
            options: CarouselOptions(
              aspectRatio: 2.7,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
              autoPlay: false,
              onPageChanged: (index, i) {
                setState(() {
                  _currentSlider = index;
                });
              },
            ),

            items: slideImgList.map(
                  (address) {

                return GestureDetector(
                  onTap:()=> MyApp.pushPage(route: ProductSingle(address), context: context),
                  child: Container(
                    height: height/5,
                    width: width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),

                      ),
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        child: loadingImage||files[getFileIndex(address)]==null?
                            Center(child: CircularProgressIndicator()):Image.file(
                              files[getFileIndex(address)],
                              fit: BoxFit.cover,
                          height: height/5,
                          width: width,

                        )
                    ),
                  ),
                );
              },
            ).toList(),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: map<Widget>(
              slideImgList,
                  (index, url) {
                return Container(
                  width: 7.0,
                  height: 7.0,
                  margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 5.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _currentSlider ==  index
                          ? Theme.of(context).accentColor
                          : Theme.of(context).primaryColorLight),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }
}