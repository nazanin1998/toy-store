import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:toy_store/Models/user.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'dart:convert';

import 'api-cart.dart';
import 'api-favorites.dart';
import 'api-products.dart';
import 'api-result.dart';
import 'api-users.dart';
import 'app-state.dart';


class API {
  static final String API_TOKEN = StringUtils.API_TOKEN;
  static final String JSON_CONTENT_TYPE = StringUtils.JSON_CONTENT_TYPE;
  static final String API_URL = StringUtils.API_URL;
  static final String API_URL_BASE = StringUtils.API_URL_BASE;
  static int TIME_OUT_DURATION = StringUtils.TIME_OUT_DURATION;

  static final APIUser userAPI = new APIUser();
  static final APIProduct productAPI = new APIProduct();
  static final APICart cartAPI = new APICart();
  static final APIFavorites favoriteAPI = new APIFavorites();



  static Future<APIResult> get(String partUrl) async {
    var res = await apiRequest(partUrl, "GET");
    return res;
  }

  static Future<APIResult> post(String partUrl, dynamic body) async {
    var res = await apiRequest(partUrl, "POST", postBody: body);
    return res;
  }

  static Future<APIResult> delete(String partUrl) async {
    var res = await apiRequest(partUrl, "DELETE");
    return res;
  }


  static Future<APIResult> apiRequest(String partUrl, String method,
      {dynamic postBody, dynamic headers}) async {
    User _currentUser = AppState.instance.getCurrentUser();
    String USER_TOKEN = null;
    if (_currentUser != null && _currentUser.token!= null) {
      USER_TOKEN = _currentUser.token;
    }
    print("user token in api.dart");
    print(USER_TOKEN);
    String url = API_URL + partUrl;
    final EncodedBody = jsonEncode(postBody);

    _log("REQUEST(" + method + ") => " + API_URL + partUrl);

    try {
      http.Response response = null;
      if (method == "GET") {
        response = await http.get(
          url,
          headers:
          USER_TOKEN==null?
          {
//            'token': USER_TOKEN,
          }:
          {
            'token': USER_TOKEN,
          },
        ).timeout(Duration(seconds: TIME_OUT_DURATION));
        _log("RESPONSE => " + response.body.toString());
      } else if(method == "POST") {
        response = await http
            .post(url,
            headers:
            USER_TOKEN==null?
            {
              "content-type":"application/json"
            }:
            {
              'token': USER_TOKEN,
              "content-type":"application/json"

            }
            , body: EncodedBody)
            .timeout(Duration(seconds: TIME_OUT_DURATION));
        _log("RESPONSE => " + response.body.toString());
      } else {
        response = await http
            .delete(url,
            headers:
            USER_TOKEN==null?
            {
              "content-type":"application/json"
            }:
            {
              'token': USER_TOKEN,
              "content-type":"application/json"

            })
            .timeout(Duration(seconds: TIME_OUT_DURATION));
        _log("RESPONSE => " + response.body.toString());
      }

      Map<String, dynamic> jsData = jsonDecode(response.body);
      var result = new APIResult(code: response.statusCode);
      result.code = jsData['code'];
      result.error = jsData['error'];

      if (result.code != 200 || result.error != null) {
        result.succeed = false;
        result.data = null;
      } else {
        result.succeed = true;
        result.error = null;
        result.data = jsData['data'];
      }


      return result;
    } catch (err, msg) {
      var e = "Unable to get response " + msg.toString();
      var code = -500;
      if (err is TimeoutException) {
        e = "خطا در دسترسی به سرور ";
      }
      if (err.toString().contains("SocketException") || err.toString().contains("Connection failed")) {
        code = -1;
        e = "اینترنت موجود نیست ";
      }
      _log(e);
      print("Error => " + err.toString());
      var result =
      new APIResult(
          error: err.toString(), succeed: false, data: null, code: code);
      return result;
    }
  }
}

void _log(dynamic msg) {
  print("API Call=>" + msg);
}

void largeLog(String content) {
  if (content.length > 4000) {
    print("Large Log " + content.substring(0, 4000));
    print("Large Log " + content.substring(4000));
  } else
    print(content);
}
