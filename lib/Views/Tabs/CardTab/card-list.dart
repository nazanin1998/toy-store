import 'package:flutter/cupertino.dart';
import 'package:toy_store/Models/product.dart';

import 'add-to-card-card.dart';

class CardList extends StatelessWidget {
  List<Product> slideImgList = [];

  CardList(this.slideImgList);
  @override
  Widget build(BuildContext context) {
    return ListView.builder
      (
        padding: EdgeInsets.all(20),
        itemCount: slideImgList.length,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext ctxt, int index) {
          return AddToCardCard(slideImgList[index]);
        }
    );

  }
}
