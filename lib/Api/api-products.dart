
import 'package:toy_store/Models/product.dart';

import 'api-result.dart';
import 'api.dart';

class APIProduct {

  Future<SingleProductAPIResult> getProduct(dynamic id) async {
    try {
      var resp = await API.get("products/?id=" + id.toString());
      var result = new SingleProductAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var productData = result.data;
      if (productData.length != 0)
        result.product = Product.fromJson(productData[0]);
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleProductAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<MultipleProductAPIResult> getAllProducts(String params) async {
    try {
      var resp = await API.get("products/" + params);
      var result = MultipleProductAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;

      var productsList = result.data;
      result.products = [];
      if (productsList.length != 0) {
        for (var i = 0; i < productsList.length; i++) {
          result.products.add(Product.fromJson(productsList[i]));
        }
      }
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new MultipleProductAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleProductAPIResult> incrementProductView(String productId) async {
    try {
      var resp = await API.get("products/product/view?id=" + productId);
      var result = SingleProductAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var productData = result.data;
      if (productData != '' || productData != null)
        result.product = Product.fromJson(productData);
      return result;

    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleProductAPIResult(500, msg.toString());
      return response;
    }
  }

}

class SingleProductAPIResult extends APIResult {
  Product product;

  SingleProductAPIResult(int code, String error) : super();

  SingleProductAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  @override
  String toString() {
    return super.toString() + "Product:${product.toString()}";
  }
}

class MultipleProductAPIResult extends APIResult {
  List<Product> products;

  MultipleProductAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  MultipleProductAPIResult(int code, String error) : super();

  @override
  String toString() {
    return super.toString() + "Products:${products.toString()}";
  }
}
