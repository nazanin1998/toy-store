import 'dart:ui';

class Product{

  String id;
  String title;//max_length=100
  String description;
  String gender;
  String brand;
  String ageRange;
  String category;
  String addedAt;//"%Y/%m/%d"
  String modifiedAt;//"%Y/%m/%d"
  String background;

  int isbn;//code of product
  int stock;//mojudi
  int bought;//default=0
  int localAddToCardQuantity=0;//default=0
  int likes;//default=0
  int views;//default=0

  bool isAvailable;
  double price;
  double weight;

  List<String> images=[];
  List<String> dimensions=[];
  List<String> colors=[];



  Product.fromJson(dynamic js){

    print("products js is");
    print(js);
    this.id = js['_id'];
    this.title = js['title'];
    this.description = js['description'];
    this.gender = js['gender']=="boy"?"پسرانه":"دخترانه";
    this.brand = js['brand'];
    this.ageRange = js['age_range'];
    this.category = js['category'];
    this.addedAt = js['added_at'];
    this.modifiedAt = js['modified_at'];
    this.background = js['background'];

    this.isbn = js['isbn'] as int;
    this.stock = js['stock'] as int;
    this.bought = js['bought'] as int;
    this.likes = js['likes'] as int;
    this.views = js['views'] as int;

    this.isAvailable = js['is_available'] as bool;

    this.price = js['price'] as double;
    this.weight = js['weight'] as double;

    var imagesJs = js['images'];
    var colorsJS = js['colors'];//todo colors list
    var dimensionsJS = js['dimensions'];

    for (var i = 0; i < imagesJs.length; i++)
      this.images.add(imagesJs[i]);

    for (var i = 0; i < colorsJS.length; i++)
      this.colors.add(colorsJS[i]);

    for (var i = 0; i < dimensionsJS.length; i++)
      this.dimensions.add(dimensionsJS[i]);


  }


  dynamic toJson(){

    var imagesJs = [];
    var dimensionsJs = [];
    var colorsJS = [];

    for (var i = 0; i < images.length; i++)
      imagesJs.add(images[i]);

    for (var i = 0; i < colors.length; i++)
      colorsJS.add( colors[i]);

    for (var i = 0; i < dimensions.length; i++)
        dimensionsJs.add( dimensions[i]);


    return {
      '_id' : id,
      'title' : title,
      'description' : description,
      'gender': gender,
      'brand': brand,
      'age_range': ageRange,
      'category': category,
      'added_at': addedAt,
      'modified_at': modifiedAt,
      'background': background,
      'isbn': isbn,
      'stock': stock,
      'bought': bought,
      'likes': likes,
      'views': views,
      'is_available': isAvailable,
      'price': price,
      'weight': weight,
      'images': imagesJs,
      'dimensions': dimensionsJs,
      'colors': colorsJS,
    };
  }
  @override
  String toString() {
    return super.toString()+toJson().toString();
  }
//  Product(this.title, this.description, this.gesex, this.brand, this.ageRange, this.category,
//      this.stock, this.bought, this.isAvailable, this.price, this.weight , this.colors, this.backgroundColor, this.images, this.localAddToCardQuantity);
}
