import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toy_store/Api/api.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/SignupLogin/login.dart';
import 'package:toy_store/Views/Utils/custom-circular-button.dart';
import 'package:toy_store/Views/Utils/custom-snack-bar.dart';
import 'package:toy_store/Views/Utils/test.dart';

import '../main-navigation.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _hasConnection = false;
  bool _loading = true;
  bool _bottomLoading = false;
  bool _clicked = false;
  String _error = "";

  @override
  void initState() {
    super.initState();
    boot();
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    double height = MediaQuery
        .of(context)
        .size
        .height;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: width,
        child: Column(
          children: <Widget>[
            Image.asset(
              'assets/images/logo.jpg',
              width: MediaQuery.of(context).size.width/4*3,
              height: MediaQuery.of(context).size.height/6*5,
              fit: BoxFit.fitWidth,
            ),
            _loading?SpinKitChasingDots(
              color: Theme.of(context).accentColor,
              size: 50.0,
            ):
            _reloadWidget(height, width, ()=>boot())
          ],
        ),
      )
    );

  }

  Widget _reloadWidget(double height, double width, Function callback) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: _clicked ? null : Theme
                .of(context)
                .accentColor,
            border: Border.all(color: Theme
                .of(context)
                .accentColor, width: 2)),
        height: 45,
        width: width / 3,
        child: Center(
            child: _bottomLoading
                ? SpinKitThreeBounce(
              color: _clicked
                  ? Theme
                  .of(context)
                  .accentColor
                  : Theme.of(context).primaryColorLight,
              size: 30.0,
            )
                : Text(
              "تلاش مجدد",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: _clicked
                      ? Theme
                      .of(context)
                      .accentColor
                      : Theme.of(context).primaryColorLight,
                  fontSize: 13),
            )),
      ),
      onTap: () {
        _bottomLoading = true;
        callback();
      },
      onTapCancel: () {
        setState(() {
          _clicked = false;
        });
      },
      onTapDown: (_) {
        setState(() {
          _clicked = true;
        });
      },
      onTapUp: (_) {
        setState(() {
          _clicked = false;
        });
      },
    );
  }

  void boot()async{

    await AppState.init();

    var newProducts = await API.productAPI.getAllProducts("new");
    if(newProducts.isSuccess()){
      AppState.instance.setNewestProducts(newProducts.products);
      var currentUser = AppState.instance.getCurrentUser();

      if(currentUser != null){
        print("Current User exits.");
        var body = {
          "password": currentUser.password,
          "email": currentUser.email
        };
        var response = await API.userAPI.login(body);
        if(response.isSuccess()){
          setState(() {
            _loading = true;
            _error = "";
          });

          AppState.instance.setCurrentUser(response.user);
      }else{
          _error = response.error;
          AppState.instance.setCurrentUser(response.user);

        }


    }else{
      //user not save => redirect to login/register page
      print("Current User doesn't exits.");

    }
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => MainNavigation(),));
    }else{
      _error = newProducts.error;
      setState(() {
        _loading = false;
        _bottomLoading = false;
        _error = StringUtils.errorStringHandler(_error);
      });
      if(newProducts.code == -1){
        showFloatingFlushbar(context,
            msg: StringUtils.errorStringHandler(_error),
            isDismissible: true,
            baseColor: Colors.redAccent,
            icon: Icons.error);
      }
      Future.delayed(const Duration(milliseconds: 1000), () {

      });
    }



  }
}
