import 'package:toy_store/Models/cart.dart';
import 'address.dart';

class Order{
  String status;
  String orderedAt;
  String modifiedAt;
  String receiptNo;

  Cart cart;
  Address address;

  Order.fromJson(dynamic js){


    this.status = js['status'];
    this.orderedAt = js['ordered_at'];
    this.modifiedAt = js['modified_at'];
    this.receiptNo = js['receipt_NO'];
    this.cart = Cart.fromJson(js['cart']);
    this.address = Address.fromJson(js['address']);

  }
  dynamic toJson(){

    return {

      'status' : status,
      'ordered_at' : orderedAt,
      'modified_at': modifiedAt,
      'receipt_NO': receiptNo,
      'cart': cart.toJson(),
      'address': address.toJson(),
    };
  }
  @override
  String toString() {
    return super.toString()+toJson().toString();
  }
}
