import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'Tabs/CardTab/card-tab.dart';
import 'Tabs/ExploreTab/explore-tab.dart';
import 'Tabs/HomeTab/home-tab.dart';
import 'Tabs/ProfileTab/profile-tab.dart';

class MainNavigation extends StatefulWidget {
  @override
  _MainNavigationState createState() => _MainNavigationState();
}

class _MainNavigationState extends State<MainNavigation> {
  int currentIndex = 0;

  final List<Widget> _mainWidgets = [
    HomeTab(),
    ExploreTab(),
    CardTab(),
    ProfileTab()
  ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: _mainWidgets[currentIndex],
      bottomNavigationBar: BottomNavyBar(

        selectedIndex: currentIndex,
        showElevation: true,
        itemCornerRadius: 25,

        curve: Curves.easeInBack,
        onItemSelected: (index) => setState(() {
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(FontAwesomeIcons.home,size: 20,),
            title: Text('خانه'),
            activeColor: Theme.of(context).accentColor,
            inactiveColor: Theme.of(context).unselectedWidgetColor,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(FontAwesomeIcons.search,size: 20,),
            title: Text('جستجو'),
            activeColor:Theme.of(context).accentColor,
            textAlign: TextAlign.center,
            inactiveColor: Theme.of(context).unselectedWidgetColor,

          ),
          BottomNavyBarItem(
            icon: Icon(FontAwesomeIcons.shoppingBag,size: 20,),
            title: Text("سبد خرید"),
            activeColor: Theme.of(context).accentColor,
            inactiveColor: Theme.of(context).unselectedWidgetColor,

            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(FontAwesomeIcons.user,size: 20,),
            title: Text('پروفایل'),
            activeColor: Theme.of(context).accentColor,
            inactiveColor: Theme.of(context).unselectedWidgetColor,
            textAlign: TextAlign.center,
          ),
        ],
      ),

    );
  }
}

