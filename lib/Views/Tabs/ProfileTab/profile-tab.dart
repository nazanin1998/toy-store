import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/Models/user.dart';
import 'package:toy_store/views/Product/product-horizontal-card.dart';
import 'package:toy_store/views/Tabs/ProfileTab/your_buy_list.dart';
import 'package:toy_store/views/Utils/circle-tab-indicator.dart';

class ProfileTab extends StatefulWidget {
  @override
  _ProfileTabState createState() => _ProfileTabState();
}


class _ProfileTabState extends State<ProfileTab>  with
    TickerProviderStateMixin{
  TabController _tabController;

  User currentUser;

  final List<Widget> myTabs = [
    Tab(
      text: "خرید های شما",
    ),
    Tab(
      text: "لیست مورد علاقه",
    ),
    Tab(
      text: "پرداخت ها",
    ),
  ];

  int _tabIndex = 0;

  _ProfileTabState(){
    currentUser = AppState.instance.getCurrentUser();
  }
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }


  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _tabIndex = _tabController.index;
      });
    }
  }

  @override
  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
    _tabController.addListener(_handleTabSelection);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ListView(
      children: <Widget>[
        _imageRow(height, width),

        Container(
          width: width,
          child: Center(
            child: TabBar(
              indicator: CircleTabIndicator(color:Colors.teal, radius: 3),
              controller: _tabController,
              labelColor: Colors.teal,
              unselectedLabelColor: Colors.black54,
              isScrollable: true,
              tabs: myTabs,
            ),
          ),
        ),
        Center(
          child: [
            YourBuyGrid(),
            ListView.builder(
              physics:NeverScrollableScrollPhysics() ,
              shrinkWrap: true,
              itemCount: 10,
              itemBuilder: (context, index) {
//                return ListTile(
//                  title: ProductHorizontalCard(),
//                );
              },
            ),
            Center(child: Text("پرداختی وجود ندارد"),)
          ][_tabIndex],
        ),
      ],
    );
  }

  Widget _mytab(double height){
    return Column(
      children: <Widget>[


      ],
    );
  }
  Widget _imageRow(double height,double width ){
    return Stack(
      children: <Widget>[
        Container(
          height: height/5,
          width: width,
          margin: EdgeInsets.only(right: 10, left: 10, top: height/16+height/32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft :Radius.circular(30), topRight:Radius.circular(30)),
            color: Colors.black12,

          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding:  EdgeInsets.only(top:height/16+height/32 , bottom: height/64,),
                child:
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(currentUser==null?"نام و نام خانوادگی":currentUser.firstName+" "+currentUser.lastName, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                  ],
                ),

              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("موجودی: 1000 ریال", style: TextStyle(fontWeight: FontWeight.normal, fontSize: 13),),
                ],
              ),

            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.all(height/32),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(height/16) ,
                  child:Image.asset('assets/images/profile.png',color:Colors.grey,height: height/8, width: height/8, fit: BoxFit.cover,),
                ),
                decoration: BoxDecoration(
                    color:Theme.of(context).backgroundColor,
                    border: Border.all(width: 1, color: Colors.grey),

                    borderRadius: BorderRadius.all(Radius.circular(height/16))
                ),
              )

            ],
          ),
        )
      ],
    ) ;
  }
}
