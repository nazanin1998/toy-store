import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showFloatingFlushbar(BuildContext context,
    {String msg = "لاگین موفقت آمیز",
      IconData icon = Icons.airline_seat_flat,
      int miliTime =2500,
      Color baseColor = const Color.fromRGBO(254, 211, 44, 1),
      bool shouldIconPulse = false,bool isDismissible =false}) {
  Flushbar(
    margin: const EdgeInsets.all(10.0),
    borderRadius: 5,
    backgroundColor: baseColor,
    boxShadows: [
      BoxShadow(
        color: Colors.black45,
        offset: Offset(3, 3),
        blurRadius: 3,
      ),
    ],
    shouldIconPulse: shouldIconPulse,
    isDismissible: isDismissible,
    duration:  Duration(milliseconds: miliTime),
    dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    icon: Icon(
      icon,
      color: Colors.white,
    ),
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    message: msg,
  )..show(context);
}
