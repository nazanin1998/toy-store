import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;


    return Container(
        margin: EdgeInsets.only(left: 5, right: 5),
        width: _width/5*2,
        decoration: BoxDecoration(

          border: Border.all(color:Colors.grey.withOpacity(0.2), width: .5),
            color:Theme.of(context).cardColor,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all( Radius.circular(20))
        ),
        child: Column(
          children: <Widget>[
            _clippedImage(_height, _width),
            _textRow(_height,_width, Theme.of(context).textSelectionColor, "ماشین کنترلی", true),
            _textRow(_height,_width, Theme.of(context).textSelectionColor,"دارای رنگ های متفاورت و سرعت بالا", false),
            Expanded(child: Column(),),
            _priceRow(_height,_width, Theme.of(context).highlightColor,"تومان2,400,000", true),
            _priceRow(_height,_width, Colors.pink,"تومان2,000,000", false),

          ],
        )
    );
  }

  Widget _clippedImage(double height, double width){
    return Padding(
      padding: const EdgeInsets.only(bottom: 7.5),
      child: ClipRRect(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight:Radius.circular(20)),
          clipBehavior: Clip.antiAlias,
           child: Image.asset("assets/images/slide2_.jpg",
             fit: BoxFit.cover,
             height: height/6 ,
             width: width/5*2,
           ),
      ),
    );
  }
  Widget _priceRow(double height, double width,Color color,String text, bool lineThrough){
    return
      Padding(
        padding: const EdgeInsets.only(left:7.5,right: 7.5),
        child: Row(
          children: <Widget>[
            Expanded(child: Row(),),

            Container(
              height:height/28,
              child: Text(text,textDirection: TextDirection.rtl,textAlign: TextAlign.right,style: TextStyle(decoration: lineThrough?TextDecoration.lineThrough:TextDecoration.none,fontSize: lineThrough?10:12, color: color ), maxLines: 1, overflow: TextOverflow.ellipsis,),
            ),

          ],
        ),
      );
  }
  Widget _textRow(double height, double width,Color color,String text, bool bold){
    return Padding(
        padding: const EdgeInsets.only(left:7.5, right: 7.5),
        child: SizedBox(
          height:height/28,
          width: width/5*2,
          child: AutoSizeText(
            text,
          overflow: TextOverflow.ellipsis,
            style: TextStyle(color:color,fontSize: 13.0, fontWeight: bold?FontWeight.bold:FontWeight.normal,),
            maxLines: 1,
          ),
        ),
      );
    //            Padding(
//              padding: const EdgeInsets.only(left:7.5, right: 7.5),
//              child: SizedBox(
//                  height:15,
//                  width: _width/5*2,
//                  child: Text("دارای رنگ های متفاورت و سرعت بالا",style: TextStyle(color:Theme.of(context).textSelectionColor,fontSize: 13 ), maxLines: 1, overflow: TextOverflow.ellipsis,),
//                ),
//            ),
  }
}
