import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:toy_store/DataServe/product-data-serve.dart';

import '../../../main.dart';

class ExploreTab extends StatefulWidget {
  @override
  _ExploreTabState createState() => _ExploreTabState();
}


class _ExploreTabState extends State<ExploreTab> {

  String _search = "";
  final searchController = TextEditingController();


  List<String> catImage = [
    'assets/images/cat1.jpg',
    'assets/images/cat2.jpg',
    'assets/images/cat3.jpg',
    'assets/images/cat4.jpg',
    'assets/images/cat5.jpg',
    'assets/images/cat6.jpg',
    'assets/images/cat7.jpg',
    'assets/images/cat8.jpg',
  ];

  List<String> catStr = [
    "هنری وکاردستی",
    "ساختنی",
    "جنگی",
    "پازل",
    "کلکسیونی",
    "عروسک",
    "آلات موسیقی",
    "اسباب بازی فصلی"
  ];


  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,

        body:NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                    backgroundColor: Theme.of(context).accentColor,
                    expandedHeight: height/5,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      titlePadding:  EdgeInsets.only(right: 20, bottom: 20),
                      title: Text("جستجو",
                          style: TextStyle(
                              color: Theme.of(context).primaryColorLight,
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold
                          ),),

                    ),
                ),

              ];
            },
            body:Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(right: width/16, left: width/16),
                    color: Theme.of(context).accentColor,
                    width: width,
                    height: height/4,
                    child:_inputBox(height, width, searchController, "جستجو"),
                  ),
                  _gridView(height, width)
                ],

              ),

        ));

  }


  Widget _gridView(double height, double width){

    return
      GridView.count(

        padding: EdgeInsets.only(top: height/6, left: 10, right: 10, bottom: 10),
        crossAxisCount: 2,
        children: List.generate(catImage.length, (index) {
          return Container(
            margin: EdgeInsets.all(10),

            child: GestureDetector(
              onTap: (){
                MyApp.pushPage(context: context, route: ProductDataServer(params:"search?category="+catStr[index],isVertical: true,));
              },
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  ClipRRect(
                      clipBehavior: Clip.antiAlias,
                      borderRadius: BorderRadius.circular(5) ,

                      child:Image.asset(catImage[index],height: height, width: width, fit: BoxFit.cover,)
                  ),
                  Container(
                    height: height, width: width,
                    decoration: BoxDecoration(
                      color: Colors.black26,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  SizedBox(
                      width: width/3,
                      height: width/3,
                      child: Center(
                        child: AutoSizeText(
                          catStr[index],
                          style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.white),
                        ),
                      )
                  )
                ],
              ),
            ),
            decoration: BoxDecoration(

              borderRadius: BorderRadius.circular(5),
            ),
          );
        }),
      );
  }

  Widget _inputBox(double _height, double _width,
      TextEditingController controller, String lable) {
    double SIDE_WIDTH = 0.5;
    double CIRCULAR_WIDTH = 5;
    return Padding(
            padding: EdgeInsets.only(top: _height / 25),
            child: Container(
              child: TextFormField(
                controller: controller,
                cursorColor: Theme.of(context).accentColor,
                style: TextStyle(color: Theme.of(context).accentColor),
                decoration: InputDecoration(

                  hintStyle: TextStyle(
                      color: Theme.of(context).accentColor.withOpacity(0.8), fontSize: 12),
                  hintText: lable,
                  errorStyle: TextStyle(
                    fontSize: 12.0,
                  ),
                  prefixIcon: Icon(FontAwesomeIcons.search),
                  fillColor: Theme.of(context).primaryColorLight,
                  filled: true,
                  contentPadding: EdgeInsets.all(15.0),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(CIRCULAR_WIDTH),
                    borderSide: BorderSide(
                      width: SIDE_WIDTH,
                      color: Theme.of(context).accentColor,
                    ),
                  ),

                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(CIRCULAR_WIDTH),
                    borderSide: BorderSide(
                      width: SIDE_WIDTH,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(CIRCULAR_WIDTH),
                    borderSide: BorderSide(
                      width: SIDE_WIDTH,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(CIRCULAR_WIDTH),
                    borderSide: BorderSide(
                      width: SIDE_WIDTH,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(CIRCULAR_WIDTH),
                    borderSide: BorderSide(
                      width: SIDE_WIDTH,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                onChanged: (String val) {
                    _search = val;
                },
              ),
            ),
          );
  }

}


