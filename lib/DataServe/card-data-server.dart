import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toy_store/Api/api.dart';
import 'package:toy_store/Models/cart.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/Tabs/CardTab/card-list.dart';
import 'package:toy_store/Views/Utils/custom-error-widget.dart';
class CartDataServe extends StatefulWidget {
  String params;
  bool isVertical;

  CartDataServe({this.params="", this.isVertical=true});
  @override
  _CartDataServeState createState() => _CartDataServeState(params, isVertical);
}

class _CartDataServeState extends State<CartDataServe> {
  String _params;
  String error = "";

  bool isLoadingState = true;
  bool isVertical;

  Cart cart;
  List<Product> product=[];

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  _CartDataServeState(this._params, this.isVertical){
    getProducts();
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    // offset = 0;
    getProducts();
  }

  void _onLoading() async{
    await Future.delayed(Duration(milliseconds: 1000));
    // offset = offset + limit;
    getProducts();
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;

    if(error != "") {
      return isVertical?Scaffold(
          body: Center(child:CustomErrorWidget(error: StringUtils.errorStringHandler(error)))
      ):CustomErrorWidget(error: StringUtils.errorStringHandler(error),
          callBack: () => getProducts());
    }else{
      if(isLoadingState){
        return isVertical?Scaffold(
          body: Center(child: CircularProgressIndicator(),),
        ):Container(height:(height-height/5),child: Center(child: CircularProgressIndicator()));
      }else if(cart!=null && cart.items.length == 0) {

        return isVertical?Scaffold(
          body:Center(child: CustomErrorWidget(
            error: StringUtils.NO_PRODUCTS_FOUND, callBack: () => getProducts(),)) ,
        ):Container(height: (height - height / 5),
            child: Center(child: CustomErrorWidget(
              error: StringUtils.NO_PRODUCTS_FOUND, callBack: () => getProducts(),)));
      }else{

        return
        Text("cnnk");
      }
    }
  }



  getProducts()async{
    var result = await API.cartAPI.getCard(_params);
    if(result.isSuccess()){
      setState(() {
//        addProducts(result.products);
        isLoadingState = false;
      });
    }else{
      setState(() {
        isLoadingState = false;
        error = result.error;

      });
    }

  }

  addProducts(List<Product> items){
//    if(products!=null) {
//      print("OLD APP_STATE PRODUCTS LENGTH IS " + products.length.toString());
//    }else{
//      products = [];
//    }
//    if(items.length == 0){
//      return;
//    }else{
//      for(var i = 0 ; i < items.length ; i++){
//        bool hasItem =false;
//        for(var j = 0 ; j < products.length ; j++){
//          if(products[j].id == items[i].id){
//            hasItem = true;
//          }
//        }
//        if(!hasItem)
//          products.add(items[i]);
//      }
//      print("ADD NEW PRODUCTS");
//      print("NEW APP_STATE PRODUCTS LENGTH IS "+ products.length.toString());
//    }
  }
}
