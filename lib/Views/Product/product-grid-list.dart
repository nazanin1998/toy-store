import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Views/Product/product-horizontal-card.dart';

class ProductGridList extends StatelessWidget {

  List<Product> products = [];
  ProductGridList(this.products);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
//        title: ,
      ),
      body: GridView.count(
        shrinkWrap: true,

        padding: EdgeInsets.all(10),
        crossAxisCount: 2,
        physics: NeverScrollableScrollPhysics(),
        childAspectRatio: 9/12,
        children: List.generate(products.length, (index) {

          return ProductHorizontalCard(products[index]);
        }),
      ),
    );
  }
}



