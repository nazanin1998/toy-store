import 'package:toy_store/Models/product.dart';

class Item{

  String productId;
  Product product;
  int quantity;

  Item.fromJson(dynamic js){


    this.productId = js['product_id'];
    if(js['product']!=null)
    this.product = Product.fromJson(js['product']);
    this.quantity = js['quantity'] as int;

  }
  dynamic toJson(){

    return {

      'product_id' : productId,
      'product' : product,
      'quantity' : quantity,

    };
  }
  @override
  String toString() {
    return super.toString()+toJson().toString();
  }
}