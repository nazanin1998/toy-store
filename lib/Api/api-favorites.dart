import 'package:toy_store/Models/user.dart';

import 'api-result.dart';
import 'api.dart';

class APIFavorites {

  Future<SingleUserAPIResult> addToFavorite(dynamic body) async {
    try {
      var resp = await API.post("favorites/", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData["user"]);
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> removeFromFavorite(String productId) async {
    try {
      var resp = await API.delete("favorites/?product_id="+ productId);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData["user"]);
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> getFavorites(dynamic body) async {
    try {
      var resp = await API.post("favorites/", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData["user"]);
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }




}

class SingleUserAPIResult extends APIResult {
  User user;

  SingleUserAPIResult(int code, String error) : super();

  SingleUserAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  @override
  String toString() {
    return super.toString() + "user:${user.toString()}";
  }
}
