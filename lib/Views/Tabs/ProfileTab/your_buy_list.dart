import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class YourBuyGrid extends StatelessWidget {
  List<String> catImage = [
    'assets/images/cat1.jpg',
    'assets/images/cat2.jpg',
    'assets/images/cat3.jpg',
    'assets/images/cat4.jpg',
    'assets/images/cat5.jpg',
    'assets/images/cat6.jpg',
    'assets/images/cat7.jpg',
    'assets/images/cat8.jpg',
  ];

  List<String> catStr = [
    "مداد رنگی",
    "لگو",
    "آدم آهنی",
    "پازل",
    "منچ",
    "عروسک نوزاد",
    "سنتور",
    "خانه چوبی"
  ];
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return GridView.count(
      shrinkWrap: true,

      padding: EdgeInsets.all(10),
      crossAxisCount: 3,
      physics: NeverScrollableScrollPhysics(),
      childAspectRatio: 9/12,
      children: List.generate(catImage.length, (index) {
        return Container(
          margin: EdgeInsets.all(5),


          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              ClipRRect(
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(20) ,

                  child:Image.asset(catImage[index],height: height, width: width, fit: BoxFit.cover,)
              ),
              Container(
                height: 40, width: width,
                decoration: BoxDecoration(
                  color: Colors.black45,
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20)),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                      width: width/2-50,
                      child: Center(
                        child: AutoSizeText(
                          catStr[index],
                          maxLines: 1,
                          minFontSize: 10,
                          overflow: TextOverflow.ellipsis,

                          style: TextStyle(fontSize:13,fontWeight: FontWeight.bold,color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                      width: width/2-60,
                      child: Center(
                        child: AutoSizeText(
                          "تومان200,000",
                          maxLines: 1,
                          minFontSize: 10,
                          overflow: TextOverflow.ellipsis,

                          style: TextStyle(fontSize:9,fontWeight: FontWeight.bold,color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: Colors.yellow,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.circular(20),
//            boxShadow: BoxShadow.lerpList(12, 12, 2)
          ),
        );
      }),
    );
  }
}
