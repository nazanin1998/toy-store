import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Utills/file-utills.dart';
import 'package:toy_store/Views/Product/product-single.dart';
import 'package:toy_store/Views/Utils/hex-color.dart';

class ProductHorizontalCard extends StatefulWidget {

  Product product;

  ProductHorizontalCard(this.product);
  @override
  State<StatefulWidget> createState()=> _StateProductHorizontalCard(product);

}
class _StateProductHorizontalCard extends State<ProductHorizontalCard>{
  File file;
  Product product;


  _StateProductHorizontalCard(this.product){
    _loadFile();
  }
  void _loadFile()async{
    file = await FileUtils.loadBase(product.images[0], product.id+ new DateTime.now().toString().replaceAll(" ", "-")+"0.png");
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Padding(
      padding: EdgeInsets.only(right:10, left:10),
      child: GestureDetector(
        onTap: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProductSingle(product),));
        },
        child: Container(
          width: width/2.5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _image(width),
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 5),
                child: Text(product.title,  maxLines: 1,overflow: TextOverflow.ellipsis,style: TextStyle(color: Theme.of(context).textSelectionColor, fontWeight: FontWeight.bold, fontSize: 14),),
              ),
              Text(product.description, maxLines: 3,overflow: TextOverflow.ellipsis, textDirection: TextDirection.rtl, style: TextStyle(color: Theme.of(context).textSelectionHandleColor, fontSize: 12),)
            ],
          ),
        ),
      ),
    );
  }


  Widget _image(double width){
    return Stack(
      alignment: AlignmentDirectional.center,
      children: <Widget>[
        Container(
          width: width/2.5,
          height: width/2.5,
          decoration: BoxDecoration(
              color: HexColor.fromHex(product.background),
              borderRadius: BorderRadius.all(Radius.circular(4))
          ),
        ),
        _backgroundImage(width),
        _productImage(width),
      ],
    );
  }

  Widget _productImage(double width){
    return Container(height: width/4, width: width/4,
      child: ClipRRect(
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(7) ,
        child:new CachedNetworkImage(
          imageUrl:"",

          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
          errorWidget: (context, url, error) => file==null?
          Center(child: CircularProgressIndicator()):Image.file(
              file,
              fit: BoxFit.cover,
              height: width/4, width: width/4
          ),
          fit: BoxFit.cover,
            height: width/4, width: width/4
        )
      ),
    );
  }

  Widget _backgroundImage(double width){
    return ClipRRect(
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(4) ,
        child:Image.asset('assets/images/backgroun_card.png',color:Colors.black26,height: width/2.5, width: width/2.5, fit: BoxFit.cover,),
      );
  }

}
