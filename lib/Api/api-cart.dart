
import 'package:toy_store/Models/items.dart';
import 'package:toy_store/Models/user.dart';

import 'api-result.dart';
import 'api.dart';

class APICart {


  Future<SingleUserAPIResult> addToCart(String params, dynamic body) async {
    try {
      var resp = await API.post("cart/" + params, body);
      var result = SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData);
      return result;

    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<MultipleItemAPIResult> getCard(String params) async {
    try {
      var resp = await API.get("cart/" + params);
      var result = MultipleItemAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      if(result.data!=null){
        var itemsList = result.data["items"];
        result.items = [];
        if (itemsList.length != 0) {
          for (var i = 0; i < itemsList.length; i++) {
            result.items.add(Item.fromJson(itemsList[i]));
          }
        }
      }

      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new MultipleItemAPIResult(500, msg.toString());
      return response;
    }
  }

//  Future<SingleUserAPIResult> addToCart(String params, dynamic body) async {
//    try {
//      var resp = await API.post("cart/" + params, body);
//      var result = SingleUserAPIResult.fromResponse(resp);
//      return result;
//    } catch (e, msg) {
//      print(msg.toString());
//      var response = new SingleUserAPIResult(500, msg.toString());
//      return response;
//    }
//  }
}


class SingleUserAPIResult extends APIResult {
  User user;

  SingleUserAPIResult(int code, String error) : super();

  SingleUserAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  @override
  String toString() {
    return super.toString() + "user:${user.toString()}";
  }
}

class MultipleItemAPIResult extends APIResult {
  List<Item> items;

  MultipleItemAPIResult(int code, String error) : super();

  MultipleItemAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  @override
  String toString() {
    return super.toString() + "items:${items.toString()}";
  }
}