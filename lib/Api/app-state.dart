import 'dart:convert';
import 'package:toy_store/Models/cart.dart';
import 'package:toy_store/Models/items.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Models/user.dart';
import 'package:toy_store/Utills/file-utills.dart';

import 'api.dart';


class AppState {
  static AppState instance;
  static const String FILE = "app-state.json";


  //data:
  User _currentUser=null;
  List<User> _users=[];
  List<Product> _products = [];
  List<Product> _newestProducts = [];
  //another fields for caching

  AppState();

  AppState.fromJson(dynamic js) {

    //User
    var userJs = js['appUser'];
    if(userJs!=null){
      _currentUser = User.fromJson(userJs);
      setCurrentUser(_currentUser);
    }else{
      _currentUser = null;
    }

  }

  dynamic toJson() {
    var userJs = null;
    if(_currentUser!=null)
      userJs = _currentUser.toJson();

    return {
      'appUser': userJs,
    };
  }

  static Future<bool> hasFile() async {
    return await FileUtils.isFileExists(FILE);
  }

  Future<void> saveFile() async {
    print("saving File");
    await FileUtils.saveFileStr(FILE, jsonEncode(this.toJson()));
  }

  static Future<AppState> fromFile() async {
    try {
      var str = await FileUtils.loadFileStr(FILE);
      print("AppState From File=>");
      print(str);
      var js = jsonDecode(str);
      return AppState.fromJson(js);
    } catch (err, msg) {
      print(msg.toString());
      return new AppState();
    }
  }

  static Future<void> init() async {
    instance = new AppState();
    var hasOldFile = await hasFile();
    if (hasOldFile) instance = await AppState.fromFile();
  }


  // Static data setter getter
  User getCurrentUser() {
    User results = _currentUser;
    return results;
  }

  void setCurrentUser(User user) async {

    _currentUser = user;
    if(_users.length==0) {
      addUsers([_currentUser]);
    }
    else
      _users[0]=_currentUser;
    await this.saveFile();
  }

  List<Product> getProducts() {
    List<Product> products = _products;
    return products;
  }

  List<Product> getNewestProducts() {
    List<Product> products = _newestProducts;

    return products;
  }


  addItemsToCurrentUser(List<Item> items){

    if(_currentUser!=null){
      for(var i = 0 ; i < items.length ; i++){
//        _currentUser.cart=Cart.fromJson(js)
      }
    }
  }
  addProducts(List<Product> items){
    if(_products!=null) {
      print("OLD APP_STATE PRODUCTS LENGTH IS " + _products.length.toString());
    }else{
      _products = [];
    }
    if(items.length == 0){
      return;
    }else{
      for(var i = 0 ; i < items.length ; i++){
        bool hasItem =false;
        for(var j = 0 ; j < _products.length ; j++){
          if(_products[j].id == items[i].id){
            hasItem = true;
          }
        }
        if(!hasItem)
          _products.add(items[i]);
      }
      print("ADD NEW PRODUCTS");
      print("NEW APP_STATE PRODUCTS LENGTH IS "+ _products.length.toString());
    }
  }


  setNewestProducts(List<Product> products) async {
    _newestProducts = products;

    if(AppState.instance._newestProducts!=null)
      print("NEWEST PRODUCTS ARE => " + AppState.instance._newestProducts.length.toString());
  }


  setUsers(List<User> users) {
    _users = users;
  }

  addUsers(List<User> items) {
    print("OLD APP_STATE USERS LENGTH IS "+ _users.length.toString());

    if(items.length==0)
      return;
    for (var i = 0;i<items.length;i++){
      bool hasItem =false;
      for(var j =0 ;j<_users.length;j++)
      {
        if(_compareIDs(items[i].id,_users[j].id)){
          hasItem =true;
        }
      }
      if(!hasItem)
        _users.add(items[i]);
    }
    print("ADD NEW USERS");
    print("NEW APP_STATE USERS LENGTH IS "+ _users.length.toString());

  }

 
  static Future<void> delete() async {
    var oldFile = await hasFile();

    print(oldFile.toString());
    if (oldFile)
      await FileUtils.deleteFile(FILE);

    instance = new AppState();
    await hasFile();
  }


}

bool _compareIDs(String firstId, String secondId) {
  if (firstId == secondId)
    return true;
  return false;
}
