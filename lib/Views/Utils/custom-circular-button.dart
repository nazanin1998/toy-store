import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCircularButton extends StatelessWidget{

  Color backgroundColor;
  Color textColor;
  Color borderColor;
  double elevation;
  double textSize;
  double radius;
  double height;
  double width;
  String text;
  double bordersize;
  FontWeight fontWeight;

  CustomCircularButton(this.text, this.backgroundColor, this.textColor,this.borderColor,{this.width = 120.0, this.height = 40.0, this.textSize= 13.0, this.elevation = 5.0, this.radius = 20.0, this.fontWeight = FontWeight.normal,this.bordersize=0});

  @override
  Widget build(BuildContext context) {

    return Card(
        elevation: elevation,
        color: backgroundColor,
        shape:  RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius),side: BorderSide(width: bordersize,color: borderColor)),
        child: Container(
            height: height,
            width: width,
            child: new Center(
              child: Text(text, style: TextStyle(fontWeight: fontWeight,color: textColor, fontSize: textSize,),),
            )));

  }

}
