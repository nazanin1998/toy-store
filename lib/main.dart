import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toy_store/Views/SplashScreen/splash-screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return RefreshConfiguration(
        footerTriggerDistance: 15,
        dragSpeedRatio: 0.91,
        headerBuilder: ()
        =>
            WaterDropHeader(
              waterDropColor: Theme.of(context).accentColor,
              complete: Icon(
                Icons.check,
                size: 30,
                color:Theme.of(context).accentColor,
              ),
            ),
        footerBuilder: () =>
            CustomFooter(
              height: 60,
              builder: (BuildContext context, LoadStatus mode) {
                Widget body;
                if (mode == LoadStatus.idle) {
                  body = Icon(Icons.arrow_downward, color: Theme.of(context).accentColor,);
                } else if (mode == LoadStatus.loading) {
                  body =  SpinKitRing(
                    color: Theme.of(context).accentColor,
                    lineWidth: 3,
                    size: 40,

                  );
                } else if (mode == LoadStatus.failed) {
                  body = Icon(
                    Icons.close,
                    size: 30,
                    color:Theme.of(context).accentColor,
                  );
                } else if (mode == LoadStatus.canLoading) {
                  body = Icon(
                    Icons.arrow_downward,
                    size: 30,
                    color: Theme.of(context).accentColor
                  );
                } else {
                  body = Icon(
                    Icons.arrow_upward,
                    size: 30,
                    color:Theme.of(context).accentColor,
                  );
                }
                return Container(
                  height: 55.0,
                  child: Center(child: body),
                );
              },
            ),

        enableLoadingWhenNoData: false,
        shouldFooterFollowWhenNotFull: (state) {
          // If you want load more with noMoreData state ,may be you should return false
          return false;
        },
        autoLoad: true,
        child:
        MaterialApp(
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            Locale("fa", "IR"), // OR Locale('ar', 'AE') OR Other RTL locales
          ],
          localeResolutionCallback:
              (Locale locale, Iterable<Locale> supportedLocales) {
            //print("change language");
            return locale;
          },
          locale: Locale("fa", "IR") ,// OR Locale('ar', 'AE') OR Other RTL locales,,
          title: 'فروشگاه اسباب بازی',
          theme: ThemeData(
            fontFamily: "Yekan",
            primarySwatch: Colors.deepOrange,
            primaryColor: Color.fromRGBO(241, 124, 0, 1),//dark orange for title
            accentColor: Color.fromRGBO(242, 141, 41, 1),
            backgroundColor: Color.fromRGBO(240, 240, 240, 1),
            primaryColorLight: Colors.white,
            cardColor: Color.fromRGBO(220, 220, 220, 1),
            unselectedWidgetColor: Colors.grey,
            textSelectionColor:Color.fromRGBO(30, 30, 30, 1) ,
            textSelectionHandleColor:Color.fromRGBO(70, 70, 70, 1) ,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home:SplashScreen(),
        ));
  }

  static pushPage({Widget route,  BuildContext context}){
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => route,));
  }
  static popPage({BuildContext context}){
    Navigator.of(context).pop();
  }
}
