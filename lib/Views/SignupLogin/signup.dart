
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toy_store/Api/api.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/SignupLogin/signup.dart';
import 'package:toy_store/Views/Utils/custom-circular-button.dart';
import 'package:toy_store/Views/Utils/custom-snack-bar.dart';
import 'package:toy_store/main.dart';

import '../main-navigation.dart';
class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  List<String> inputeBoxContent = ["","","","","",""];
  bool genderIsGirl = false;
  bool genderIsBoy = false;


  bool _totalValidated = false;
  bool _obscureText = true;
  bool _isClicked = false;

  final nameController = TextEditingController();
  final familyNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final phoneController = TextEditingController();
  final nationalCodeController = TextEditingController();



  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  @override
  void dispose() {
    nameController.dispose();
    familyNameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    passwordController.dispose();
    nationalCodeController.dispose();
    super.dispose();
  }

  void toggleGirl(bool newValue) => setState(() {
    genderIsGirl = newValue;
    if(genderIsGirl){
      genderIsBoy = false;
    }
    _totalValidation();
  });

  void toggleBoy(bool newValue) => setState(() {
    genderIsBoy = newValue;
    if(genderIsGirl){
      genderIsGirl= false;
    }
    _totalValidation();

  });

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: null,
      body: SingleChildScrollView(
          child: Container(
            height: _height,
            width: _width,
            child:
                Form(
                    autovalidate: false,
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: _height/10, bottom: _height/30),
                          child: Text("ایجاد حساب کاربری", style: TextStyle(color: Theme.of(context).textSelectionColor, fontSize: 18, fontWeight: FontWeight.bold),),
                        ),
                        _inputBox(_height, _width, nameController, "نام", 0, Icons.person),
                        _inputBox(_height, _width, familyNameController, "نام خانوادگی", 1,  Icons.person),
                        _inputBox(_height, _width, emailController, "ایمیل", 2,  Icons.email),
                        _inputBox(_height, _width, passwordController, "رمز عبور", 3,  Icons.remove_red_eye),
                        _inputBox(_height, _width, phoneController, "موبایل", 4,  Icons.phone_iphone),
                        _inputBox(_height, _width, nationalCodeController, "کد ملی", 5,  Icons.code),
                        Padding(
                          padding:  EdgeInsets.symmetric(horizontal: _width/5),
                          child: Row(
                            children: [
                              Checkbox(
                                  value: genderIsBoy,
                                  onChanged: toggleBoy
                              ),
                              Text("مرد"),
                              Expanded(child: Row(),),
                              Checkbox(
                                  value: genderIsGirl,
                                  onChanged: toggleGirl
                              ),
                              Text("زن"),

                            ],
                          ),
                        ),
                        Expanded(child: Column(),),
                        _RegisterBtn(_height, _width),
//                        _forgetPassword(_height, _width),
                      ],
                    ),

                )

          )),
    );
  }

  Widget _forgetPassword(double _height, double _width){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Padding(
          padding:  EdgeInsets.only(bottom: _height/10),
          child: Text("فراموشی رمز عبور", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),),
        ),
        GestureDetector(
          onTap: (){
            MyApp.pushPage(route: Signup(), context: context);
          },
          child: Padding(
            padding:  EdgeInsets.only(bottom: _height/10),
            child: Text("حساب کاربری ندارم", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),),
          ),
        ),
      ],
    );
  }
  Widget _RegisterBtn(double _height, double _width){

    return Padding(
      child: GestureDetector(
        onTap: () {

          setState(() {
            _isClicked = true;
          });
          Future.delayed(const Duration(milliseconds: 100),
                  () {
                setState(() {
                  _isClicked = false;
                });
              });
          if (_formKey.currentState.validate()) {
            _register();//todo

          }},
        onTapCancel: () {
          setState(() {
            _isClicked = false;
          });
        },
        onTapDown: (_) {
          setState(() {
            _isClicked = true;
          });
        },
        onTapUp: (_) {
          setState(() {
            _isClicked = false;
          });
        },
        child: CustomCircularButton(
          "ثبت نام",

          _totalValidated
              ?_isClicked?Colors.transparent: Theme.of(context).accentColor
              : _isClicked?Colors.transparent:Theme.of(context).accentColor.withOpacity(0.5),
          _isClicked? Theme.of(context).accentColor:Theme.of(context).primaryColorLight,
          Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
          width: _width / 7 * 5,
          height: _height / 14,
          elevation: 0,
          textSize: 18,
          radius: 5,
          bordersize: 2,
        ),
      ),
      padding: EdgeInsets.only(bottom: _height/20),
    );

  }

  Widget _inputBox(double _height, double _width,
      TextEditingController controller, String lable, int index, IconData iconData) {
    return Container(
        width: _width * 7 / 8,
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: _height / 40),
            child: TextFormField(
              onTap: () {},


              obscureText: index != 3 ? false : (_obscureText?true:false),
              controller: controller,
              cursorColor: Theme.of(context).accentColor,
              decoration: InputDecoration(
                prefixIcon:  Icon(
                  iconData,
                ),
                contentPadding: EdgeInsets.all(15.0),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Theme.of(context).accentColor,
                  ),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Colors.red,
                  ),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Colors.red,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Theme.of(context).cardColor,
                  ),
                ),

                labelText: lable,
                labelStyle: TextStyle(
                    color: Theme.of(context).accentColor, fontSize: 12),
              ),
              keyboardType: index == 2?
                  TextInputType.emailAddress
                  :index == 4 || index == 5? TextInputType.number: TextInputType.text,
              validator: index == 0 ? validateName
                  : index  == 1? validateFamilyName
                  :index == 2? validateEmail
                  :index == 3?validatePassword
                  :index == 4? validatePhone: validateMelicode,
              onChanged: (String val) {
                inputeBoxContent[index] = val;
                setState(() {
                  _totalValidation();
                });
              },
              autovalidate: false,
            ),
          )
        ]));
  }

  _totalValidation() {
    if (inputeBoxContent[0].length > 0 && inputeBoxContent[1].length > 0 &&
    inputeBoxContent[2].length > 0 && inputeBoxContent[3].length > 0 &&
    inputeBoxContent[4].length > 0 && inputeBoxContent[5].length > 0  &&
        ( genderIsGirl != false|| genderIsBoy != false)
    ) {
      setState(() {
        _totalValidated = true;
      });
    } else {
      setState(() {
        _totalValidated = false;
      });
    }
  }

  String validateName(String value) {
    if (value.length<3) {
      return "نام باید بیشتر از دو حرف باشد";
    }  else {
      return null;
    }
  }
  String validateFamilyName(String value) {
    if (value.length<3) {
      return "نام خانوادگی باید بیشتر از دو حرف باشد";
    }  else {
      return null;
    }
  }
  String validatePhone(String value) {
    if (value.length!=11 || !value.startsWith("09")) {
      return "شماره نامعتبر است";
    }  else {
      return null;
    }
  }

  String validateMelicode(String value) {
    if (value.length!=10 ) {
      return "کد ملی نامعتبر است";
    }  else {
      return null;
    }
  }

  String validateEmail(String value) {
    if (!value.contains("@")) {
      return 'ایمیل اشتباه وارد شده';
    } else if (value == "") {
      return 'هیچ مقداری وارد نشده ';
    } else {
      return null;
    }
  }

  String validatePassword(String value) {
    if (value.length < 6)
      return 'رمذ عبور باید حداقل 6 حرف باشد';
    else
      return null;
  }
  _register()async {
    var body = {
      "first_name": inputeBoxContent[0],
      "last_name": inputeBoxContent[1],
      "password": inputeBoxContent[3],
      "email": inputeBoxContent[2],
      "phone": inputeBoxContent[4],
      "national_code": inputeBoxContent[5],
      "sex": genderIsBoy?"مرد":"زن"
    };
    var result = await API.userAPI.register(body);
    if (result.isSuccess()) {
      print(result.user);
      AppState.instance.setCurrentUser(result.user);
      _formKey.currentState.save();
      SystemChannels.textInput
          .invokeMethod('TextInput.hide')
          .then((_) {});
      showFloatingFlushbar(context,
          msg: StringUtils.errorStringHandler(
              "ایجاد حساب کاربری با موفقیت انجام شد"),
          isDismissible: true,
          baseColor: Colors.green,
          icon: Icons.check);
      Future.delayed(const Duration(milliseconds: 1000), () {
         MyApp.pushPage(
            context: context, route: MainNavigation());
      });
    } else {
      showFloatingFlushbar(context,
          msg: StringUtils.errorStringHandler(result.error),
          isDismissible: true,
          baseColor: Colors.red,
          icon: Icons.error);
      Future.delayed(const Duration(milliseconds: 1000), () {

      });
    }
  }
}
