
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../../main.dart';

class CustomErrorWidget extends StatefulWidget {
  String error;
  Function callBack;
  bool boolErrr;
  bool hasButton;

  CustomErrorWidget(
      {this.error = "در حال ساخت", this.callBack, this.boolErrr = true,this.hasButton=true});

  @override
  _CustomErrorWidgetState createState() => _CustomErrorWidgetState(
      error: error, callBack: callBack, boolErrr: boolErrr,hasButton:hasButton);
}

class _CustomErrorWidgetState extends State<CustomErrorWidget>
    with SingleTickerProviderStateMixin {
  String error;
  bool boolErrr;
  bool hasButton;
  Function callBack;
  bool loading = false;
  bool _isClicked = false;


  _CustomErrorWidgetState({this.error, this.callBack, this.boolErrr,this.hasButton});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Text(
                  error,
                  style: TextStyle(
                      color: Theme.of(context).accentColor, fontSize: 15),
                )),

            hasButton?  GestureDetector(
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: _isClicked ? null : Theme.of(context).accentColor,
                    border: Border.all(
                        color: Theme.of(context).accentColor, width: 2)),
                height: height / 15,
                width: width / 7 * 3,
                child: Center(
                    child: loading
                        ? SpinKitThreeBounce(
                      color: _isClicked
                          ? Theme.of(context).accentColor
                          : Theme.of(context).primaryColorLight
                      ,
                      size: 30.0,
                    )
                        : Text(
                      "تلاش مجدد",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: _isClicked
                              ? Theme.of(context).accentColor
                              : Theme.of(context).primaryColorLight,
                          fontSize: 15),
                    )),
              ),
              onTap: () {
                setState(() {
                  loading=true;
                  _isClicked = true;
                });


                Future.delayed(const Duration(milliseconds: 50), () {
                  setState(() {
                    _isClicked = false;
                  });
                });
                Future.delayed(const Duration(milliseconds: 1000), () {
                  setState(() {
                    loading = false;
                  });
                });

                callBack();

              },
              onTapCancel: () {
                setState(() {
                  _isClicked = false;
                });
              },
              onTapDown: (_) {
                setState(() {
                  _isClicked = true;
                });
              },
              onTapUp: (_) {
                setState(() {
                  _isClicked = false;
                });
              },
            ):Container()
          ],
        ),
      ),
    );
  }
}
