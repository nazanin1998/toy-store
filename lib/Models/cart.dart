
import 'package:toy_store/Models/product.dart';

import 'items.dart';

class Cart{

  int totalQuantity;
  List<Item> items=[];

  Cart.fromJson(dynamic js){
    if(js!=null){
      var itemsJs = js['items'];

      if(itemsJs!=null)
        for (var i = 0; i < itemsJs.length; i++)
          this.items.add(Item.fromJson(itemsJs[i]));

      this.totalQuantity = js['total_quantity'] as int;

    }

  }
  dynamic toJson(){
    var itemsJS = [];

    for (var i = 0; i < items.length; i++)
      itemsJS.add( items[i].toJson());

    return {

      'items' : itemsJS,
      'total_quantity' : totalQuantity,

    };
  }
  @override
  String toString() {
    return super.toString()+toJson().toString();
  }
}