import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/main.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Views/Product/product-single.dart';

class AddToCardCard extends StatefulWidget {
  Product product;

  AddToCardCard(this.product);

  @override
  State<StatefulWidget> createState() => _AddToCardCardState(product);

}

class _AddToCardCardState extends State<AddToCardCard>{
  Product product;

  _AddToCardCardState(this.product);

  @override
  Widget build(BuildContext context) {

    double width =  MediaQuery.of(context).size.width;
    double height =  MediaQuery.of(context).size.height;

    return Container(
      margin: EdgeInsets.only(bottom: 20),
      width: width,
      height: height/4.5,
      child: GestureDetector(
        child: Stack(
          children: <Widget>[
            _backgroundCard(height, width, context),
            _image(height, width, context),
          ],
        ),
        onTap: () {
          MyApp.pushPage(context: context, route: ProductSingle(product));
        }
      ),
    );

  }

  Widget _backgroundCard(double height, double width , BuildContext context){
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[

        Container(
          width: width,
          height: height/5,
          decoration: BoxDecoration(
//            color: product.backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ),
          child: Container(
            margin: EdgeInsets.only(right: width/3+30,),
            padding: EdgeInsets.only(top: height/40, bottom: height/40),
            height: height/5,
            width: width-width/3-40-30,

            child:Column(

              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//                Expanded(child: Column(),),
                Text(product.title,
                    style: TextStyle(
                        color: Theme.of(context).primaryColorLight,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                    )),
                Text(product.brand,
                    style: TextStyle(
                        color: Theme.of(context).primaryColorLight,
                        fontSize: 15.0,
                    )),
                Row(
                  children: <Widget>[
                    Text("تعداد",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 15.0,
                        )),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(right: 4, left: 6),
                        width:width/20,
                        height:width/20,
                        child: Icon(Icons.add, size: 15,),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorLight,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              spreadRadius: 1,
                              blurRadius: 2,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                      ),
                      onTap: (){
                        setState(() {
                              product.localAddToCardQuantity++;

                          //save to appstate
                        });
                      },
                    ),
                    Text(product.localAddToCardQuantity.toString(),
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 15.0,
                        )),
                    GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(right: 6),
                        width:width/20,
                        height:width/20,
                        child: Icon(Icons.remove, size: 15,),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorLight,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              spreadRadius: 1,
                              blurRadius: 2,
                              offset: Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                      ),
                      onTap: (){
                        setState(() {
                          if(product.localAddToCardQuantity>0)
                          product.localAddToCardQuantity--;
                          //save to appstate
                        });
                      },
                    ),
                  ],
                ),
                Text(product.price.toString()+" ریال",
                    style: TextStyle(
                      color: Theme.of(context).primaryColorLight,
                      fontSize: 11.0,
                    )),
//                Expanded(child: Column(),),

              ],
            )
          )
        )
      ],
    );
  }
  Widget _image(double height, double width , BuildContext context){
    return Container(
        width: width/3,
        height: height/5,
        margin: EdgeInsets.only(right: 15),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 1,
              blurRadius: 5,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
            borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: Image.asset(product.images[0], fit: BoxFit.cover,),
        )

    );
  }
}
