import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/DataServe/card-data-server.dart';
import 'package:toy_store/Models/product.dart';

import 'card-list.dart';

class CardTab extends StatefulWidget {
  @override
  _CardTabState createState() => _CardTabState();
}


class _CardTabState extends State<CardTab> {

  List<Product> slideImgList = [];

  _CardTabState(){

  }
  @override
  Widget build(BuildContext context) {

    double width =  MediaQuery.of(context).size.width;
    double height =  MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,

        body:NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(

                    backgroundColor: Theme.of(context).backgroundColor,
                    expandedHeight: height/6,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      titlePadding:  EdgeInsets.only(right: 20, bottom: 20),
                      title: Text("سبد خرید",
                          style: TextStyle(
                              color: Theme.of(context).textSelectionColor,
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold
                          )),
                      background: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            width: 40,
                            height: 4,
                            color: Theme.of(context).accentColor,
                          )
                        ],
                      ),
                    )
                ),
              ];
            },
            body: CartDataServe()
        ));
  }

}

