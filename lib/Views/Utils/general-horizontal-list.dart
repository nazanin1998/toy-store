import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/views/Product/product-card.dart';

class GeneralhorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return Container(
      width: _width,
      margin: EdgeInsets.only(top: 0,left: 10, right: 10),
      height: _height/3,
      child: generalList(),
    );

  }
  Widget generalList(){
    return ListView.builder(
        itemCount:6,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,

        itemBuilder: (context, index) {
          return ProductCard();
        },

    );
  }
}
