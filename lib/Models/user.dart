import 'package:toy_store/Models/address.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Models/cart.dart';

import 'order.dart';

class User {

  int code;

  String id;
  String firstName;// min_length=4
  String lastName;// min_length=4
  String password;// max_length=50
  String email;
  String phone;// max_length=11
  String userType;// default='user'
  String nationalCode;// max_length=10
  String sex;
  String token;

  Cart cart=null;

  List<Order> orders=[];
  List<String> favorites=[];
  List<Address> addresses=[];



  User.fromJson(dynamic js, {String token}){

    this.id = js['_id'];
    this.token = token;
    this.firstName = js['first_name'];
    this.lastName = js['last_name'];
    this.password = js['password'];
    this.email = js['email'];
    this.phone = js['phone'];
    this.userType = js['type'];
    this.nationalCode = js['national_code'];
    this.sex = js['gender'];
    this.code = js['code'] as int;
    this.cart = Cart.fromJson(js['cart']) ;
    var ordersJs = js['orders'];
    var favoritesJs = js['favorites'];
    var addressesJs = js['addresses'];
    if(ordersJs!=null)
    for (var i = 0; i < ordersJs.length; i++)
      this.orders.add(Order.fromJson(ordersJs[i]));

    for (var i = 0; i < favoritesJs.length; i++)
      this.favorites.add(favoritesJs[i]);

    for (var i = 0; i < addressesJs.length; i++)
      this.addresses.add(Address.fromJson(addressesJs[i]));

  }
  dynamic toJson(){

    var ordersJs = [];
    var favoritesJs = [];
    var addressesJs = [];

    for (var i = 0; i < orders.length; i++)
      ordersJs.add( orders[i].toJson());

    for (var i = 0; i < favorites.length; i++)
      favoritesJs.add( favorites[i]);

    for (var i = 0; i < addresses.length; i++)
      addressesJs.add( addresses[i].toJson());

    return {
      '_id' : id,
      'first_name' : firstName,
      'last_name' : lastName,
      'password' : password,
      'email': email,
      'phone': phone,
      'type': userType,
      'national_code': nationalCode,
      'gender': sex,
      'token': token,
      'code': code,
      if(cart!=null)
      'cart': cart.toJson(),
      'orders': ordersJs,
      'favorites': favoritesJs,
      'addresses': addressesJs,
    };
  }
  @override
  String toString() {
    return super.toString()+toJson().toString();
  }

}