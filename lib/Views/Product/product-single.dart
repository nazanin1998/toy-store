import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:toy_store/Api/api.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Models/user.dart';
import 'package:toy_store/Utills/file-utills.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/SignupLogin/login.dart';
import 'package:toy_store/Views/SignupLogin/signup.dart';
import 'package:toy_store/Views/Utils/custom-snack-bar.dart';
import 'package:toy_store/Views/Utils/hex-color.dart';
import 'package:toy_store/main.dart';

class ProductSingle extends StatefulWidget {

  Product product;
  ProductSingle(this.product);

  @override
  _ProductSingleState createState() => _ProductSingleState(product);
}


class _ProductSingleState extends State<ProductSingle> {
  int _currentSlider = 0;

  bool _isClicked = false;
  bool _isLikedByCurrentUser = false;
  bool loadingImage = true;

  List<File> files = [];
  Product product;
  User currentUser;


  final CarouselController _controller = CarouselController();

  _ProductSingleState(this.product){

    currentUser = AppState.instance.getCurrentUser();
    if(currentUser!=null){
      if(currentUser.favorites.contains(product.id))
        _isLikedByCurrentUser=true;
    }
    _loadImages();
    _viewAPICall();
  }

  _loadImages()async{
    for(var i=0; i < product.images.length ; i++){
      File file= await FileUtils.loadBase(product.images[i], product.id+ new DateTime.now().toString().replaceAll(" ", "-")+i.toString()+".png");
      files.add(file);
    }
    setState(() {
      loadingImage = false;
    });

  }
  @override
  Widget build(BuildContext context) {

    double width =  MediaQuery.of(context).size.width;
    double height =  MediaQuery.of(context).size.height;

    return  Scaffold(
        body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Theme.of(context).backgroundColor,
                expandedHeight:height/2,
                floating: false,
                pinned: false,
                automaticallyImplyLeading: false,
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.parallax,
                  background: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _backRow(height, width),
                      _categorySexRow(height, width),
                      _titleRow(height, width),
                      _ratingRow(height, width),
                      _ManualCaresoul(height, width),
                    ],
                  ),),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    _bottomCarusalDetailcard(height, width)
                  ],
                ),

              ),
            ])
    );
  }

  Widget _backRow(double height , double width) {
    return GestureDetector(
      onTap: (){
        MyApp.popPage(context: context);
      },
      child: Padding(
        padding:  EdgeInsets.only(top: height/20, bottom: height/50),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20, left: 10),
              child: Icon(FontAwesomeIcons.longArrowAltRight,color:Theme.of(context).textSelectionHandleColor),
            ),
            Text(StringUtils.RETURN_TXT, style: TextStyle(color:Theme.of(context).textSelectionHandleColor),)
          ],
        ),
      ),
    );

  }

  Widget _ratingRow(double height , double width) {
    print("rating");
    print(product.views==0?0:product.likes/product.views);
    return Padding(
      padding: EdgeInsets.only(right:20 , left: 20),
      child: RatingBarIndicator(
        rating: product.views==0?0:product.likes/product.views*5,
        unratedColor: Theme.of(context).unselectedWidgetColor,
        itemBuilder: (context, index) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        itemCount: 5,
        itemSize: height/40,
        direction: Axis.horizontal,
      ),
    );
  }

  Widget _categorySexRow(double height , double width) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, height/50, 20, height/70),
      child: Row(
        children: <Widget>[
          Text(product.category, style: TextStyle(fontSize: 13, color: Theme.of(context).textSelectionHandleColor),),
          Expanded(child:Row()),
          Text(product.gender, style: TextStyle(fontSize: 13, color: Theme.of(context).textSelectionHandleColor.withOpacity(0.7))),
        ],
      ),
    );
  }

  Widget _titleRow(double height , double width) {
    return Container(
      padding: EdgeInsets.only(right:20 , left: 20),
      child: Text(product.title, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Theme.of(context).textSelectionHandleColor),),
    );
  }



  Widget _bottomCarusalDetailcard(double height , double width) {
    return SingleChildScrollView(
      child: Container(
        width: width,
        height: height/3*2,
        margin: EdgeInsets.only(top: 0),
        decoration: BoxDecoration(
            color: HexColor.fromHex(product.background),
            borderRadius: BorderRadius.only(topLeft:Radius.circular(30), topRight: Radius.circular(30))
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _priceLikeRow(),
              _descriptionRow(height),

              Divider(color: Theme.of(context).primaryColorLight.withOpacity(0.5)),

              Padding(
                padding: EdgeInsets.only(top:height/40, bottom: height/60),
                child: Text(
                  "ویژگی های محصول",
                  style: TextStyle(fontSize: 14, fontWeight:FontWeight.bold,color:Theme.of(context).primaryColorLight),
                ),
              ),
              _featureRow("بازه سنی: "+product.ageRange, height),
              _featureRow("وزن: "+product.weight.toString()+"گرم", height),
              _featureRow("برند: "+product.brand, height),
              _featureRow("ابعاد: "+product.dimensions[0]+"x"+product.dimensions[1]+"x"+product.dimensions[2], height),
              _chooseColorRow(height, width),
              _addToCardBtn(height, width)
            ],
          ),
        ),
      ),
    );

  }

  Widget _featureRow(String content, double height){

    return Padding(
      padding:  EdgeInsets.only(bottom: height/90),
      child: Text(
        content,
        style: TextStyle(fontSize: 13,color:Theme.of(context).primaryColorLight),
      ),
    );
  }

  Widget _priceLikeRow() {
    return Row(
      children: <Widget>[
        Text(product.price.toString()+" ریال", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color:Theme.of(context).primaryColorLight)),
        Expanded(child:Row()),
        IconButton(
            icon: Icon(_isLikedByCurrentUser?Icons.favorite:Icons.favorite_border, color:Theme.of(context).primaryColorLight),
            onPressed: (){
              if(currentUser==null){
                MyApp.pushPage(context: context, route: Login(fromInsideOfApp: true,));
              }else{
                //todo
                //call => add tp favorite
                _isLikedByCurrentUser?_removeFromFavorites():_addTofavorite();
              }
            },),
      ],
    );
  }

  Widget _descriptionRow(double height){
    return Padding(
      padding: EdgeInsets.only(top:height/30, bottom: height/40),
      child: Text(
        product.description,
        maxLines: null,

        textDirection: TextDirection.rtl,
        style: TextStyle(fontSize: 13, color:Theme.of(context).primaryColorLight),
      ),
    );
  }

  Widget _addToCardBtn(double height , double width) {
    return Padding(
        padding: EdgeInsets.only(top: height / 20),
        child: GestureDetector(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: _isClicked
                    ? HexColor.fromHex(product.background)
                    : Theme.of(context).primaryColorLight,
                border: Border.all(
                    color: _isClicked?Theme.of(context).primaryColorLight:Colors.transparent,
                    width: 2)
            ),
            height: height / 13,
            width: width ,
            child: Center(
                child:  Text(
                  "اضافه کردن به سبد خرید",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: _isClicked
                          ? Theme.of(context).primaryColorLight
                          : HexColor.fromHex(product.background),
                      fontSize: 15),
                )),
          ),
          onTap: () {
            setState(() {
              _isClicked = true;
            });
            Future.delayed(const Duration(milliseconds: 50),
                    () {
                  setState(() {
                    _isClicked = false;
                  });
                });

            addToCart();

          },
          onTapCancel: () {
            setState(() {
              _isClicked = false;
            });
          },
          onTapDown: (_) {
            setState(() {
              _isClicked = true;
            });
          },
          onTapUp: (_) {
            setState(() {
              _isClicked = false;
            });
          },
        ));
  }
  Widget _chooseColorRow(double height , double width) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
            padding:  EdgeInsets.only(top: height/50, bottom: height/90),
            child: Text(
              "رنگ های موجود",
              style: TextStyle(fontSize: 14, fontWeight:FontWeight.bold,color:Theme.of(context).primaryColorLight),
            )
        ),
        Container(
            height: width/12,
            width: width-80,
            child:ListView.builder
              (
                itemCount: product.colors.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext ctxt, int index) {
                  return Container(
                    width: width/12,
                    margin: EdgeInsets.only(left: 5),
                    decoration: BoxDecoration(
                        color:HexColor.fromHex(product.colors[index]),
                        border: Border.all(width: 2, color:Colors.white),
                        borderRadius: BorderRadius.all(Radius.circular(width/6))
                    ),
                  );
                }
            )
        )
      ],
    );
  }
  Widget _ManualCaresoul(double height , double width) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(icon:Icon(FontAwesomeIcons.caretRight,
                color:HexColor.fromHex(product.background)
            ),
              onPressed: (){
                _controller.previousPage();
              },),

            ClipRRect(
              borderRadius:BorderRadius.all(Radius.circular(10)) ,
              child: Container(
                margin: EdgeInsets.only(top: height/30, bottom: height/40),
                height: height/5,
                width: width-130,
                color: Colors.transparent,

                child: CarouselSlider(

                  items: files.map((item) =>

                      Container(
                        height: height/5,
                        width: width-130,

                          child: ClipRRect(
                              borderRadius:BorderRadius.all(Radius.circular(10)) ,
                              child:loadingImage||item==null?
                              Center(child: CircularProgressIndicator()):Image.file(
                                  item,
                                  fit: BoxFit.cover,
                                  height: width/4, width: width/4

                              )
                      ))).toList(),
                  options: CarouselOptions(
                    height: height/5,
                    enlargeStrategy: CenterPageEnlargeStrategy.height,
                    enlargeCenterPage: false,
                    disableCenter: true,
                    aspectRatio: 3.0,
                    viewportFraction: 1.0,
                    autoPlay: false,

                    enableInfiniteScroll:false,
                    onPageChanged: (index, i) {
                      setState(() {
                        _currentSlider = index;
                      });
                    },
                  ),
                  carouselController: _controller,
                ),
              ),
            ),
            IconButton(icon:Icon(FontAwesomeIcons.caretLeft,

              color: HexColor.fromHex(product.background)
              ),
              onPressed: (){
                _controller.nextPage();
              },),

          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(
            product.images,
                (index, url) {
              return Container(
                width: width/20,
                height: 4.0,
                margin: EdgeInsets.symmetric(horizontal: 2),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                    color: _currentSlider ==  index
                        ? HexColor.fromHex(product.background)
                        : Theme.of(context).unselectedWidgetColor),
              );
            },
          ),
        ),
      ],
    );

  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  void _viewAPICall()async{
    var response = await API.productAPI.incrementProductView(product.id);
    //todo update product caue of views count in appState
    if(response.isSuccess()){
      AppState.instance.addProducts([response.product]);
      product = response.product;
    }

  }

  void _removeFromFavorites()async{

    var response = await API.favoriteAPI.removeFromFavorite(product.id);
    if(response.isSuccess()){

      String token = currentUser.token;
      currentUser = response.user;
      currentUser.token = token;
      AppState.instance.setCurrentUser(currentUser);
      product.likes = product.likes-1;
      AppState.instance.addProducts([product]);
      setState(() {
        _isLikedByCurrentUser = false;
      });

    }else{
      showFloatingFlushbar(context,
          msg: StringUtils.errorStringHandler(response.error),
          isDismissible: true,
          baseColor: Colors.redAccent,
          icon: Icons.error);
      Future.delayed(const Duration(milliseconds: 50),
              () {

          });
    }
  }
  void _addTofavorite()async{
    var body = {
      "product_id":product.id
    };

    var response = await API.favoriteAPI.addToFavorite(body);
    if(response.isSuccess()){

      String token = currentUser.token;
      currentUser = response.user;
      currentUser.token = token;
      AppState.instance.setCurrentUser(currentUser);

      product.likes = product.likes+1;
      AppState.instance.addProducts([product]);
      setState(() {
        _isLikedByCurrentUser = true;
      });

    }else{
      showFloatingFlushbar(context,
          msg: StringUtils.errorStringHandler(response.error),
          isDismissible: true,
          baseColor: Colors.redAccent,
          icon: Icons.error);
      Future.delayed(const Duration(milliseconds: 50),
              () {

          });
    }
  }

  void addToCart()async{
    if(currentUser==null){
      MyApp.pushPage(context: context, route: Login());
    }else{

      if(_thisItemHasbeenAddedToCart(product.id)){

        showFloatingFlushbar(context,
            msg: StringUtils.errorStringHandler("قبلا به سبد خرید اضافه شده است"),
            isDismissible: true,
            baseColor: Colors.redAccent,
            icon: Icons.error);
        Future.delayed(const Duration(milliseconds: 50),
                () {

            });
      }else {
        //todo add to cart
        var body = {
          "product_id": product.id,
          "quantity": 1
        };
        var response = await API.cartAPI.addToCart("", body);
        print("response is");
        print(response);
        if(response.isSuccess()){
          String token = currentUser.token;
          response.user.token = token;
          AppState.instance.setCurrentUser(response.user);
          currentUser = AppState.instance.getCurrentUser();
          showFloatingFlushbar(context,
              msg: "به سبد خرید شما اضافه شد",
              isDismissible: true,
              baseColor: Colors.green,
              icon: Icons.check);

        }else{

          showFloatingFlushbar(context,
              msg: StringUtils.errorStringHandler(response.error),
              isDismissible: true,
              baseColor: Colors.redAccent,
              icon: Icons.error);
        }
      }
    }
  }
  bool _thisItemHasbeenAddedToCart(String id){

    for(var i = 0 ; i < currentUser.cart.items.length ; i++){
      if(currentUser.cart.items[i].productId == id)
        return true;
    }
    return false;
  }
}
