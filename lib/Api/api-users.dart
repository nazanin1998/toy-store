import 'package:toy_store/Models/user.dart';

import 'api-result.dart';
import 'api.dart';

class APIUser {
  Future<SingleUserAPIResult> login(dynamic body) async {
    try {
      var resp = await API.post("users/login", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData["user"], token:userData['token']);

      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> register(dynamic body) async {
    try {
      var resp = await API.post("users/register", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData["user"], token:userData['token']);
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> changePassword(dynamic body) async {
    try {
      var resp = await API.post("users/change-password", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData);
      else
        result.error = 'no result';
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> signUp(dynamic body) async {
    try {
      var resp = await API.post("users/new", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData.length != 0) {
        print('user data length' + userData.length.toString());
        print(userData.toString());
      }
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData);
      else
        result.error = 'no result';
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> editProfile(int id, dynamic body) async {
    try {
      var resp = await API.post("users/" + id.toString() + "/edit", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData);
      else
        result.error = 'no result';
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> getUser(dynamic id) async {
    try {
      var resp = await API.get("users/?id=" + id.toString());
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData.length != 0)
        result.user = User.fromJson(userData[0]);
      else
        result.error = 'no result';
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> forgetPassword(dynamic body) async {
    try {
      var resp = await API.post("users/forget-password", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      result.user = null;
      result.error = 'no result';
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<SingleUserAPIResult> confirmPassword(dynamic body) async {
    try {
      var resp = await API.post("users/confirm-password", body);
      var result = new SingleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;
      var userData = result.data;
      if (userData != '' || userData != null)
        result.user = User.fromJson(userData);
      else
        result.error = 'no result';
      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new SingleUserAPIResult(500, msg.toString());
      return response;
    }
  }

  Future<MultipleUserAPIResult> getAllUsers(String params) async {
    try {
      var resp = await API.get("users/" + params);
      var result = MultipleUserAPIResult.fromResponse(resp);
      if (!resp.isSuccess()) return result;

      var usersList = result.data;
      result.users = [];
      if (usersList.length != 0) {
        for (var i = 0; i < usersList.length; i++) {
          print("user :" + User.fromJson(usersList[i]).toString());
          result.users.add(User.fromJson(usersList[i]));
        }
      } else
        result.error = 'no result';

      return result;
    } catch (e, msg) {
      print(msg.toString());
      var response = new MultipleUserAPIResult(500, msg.toString());
      return response;
    }
  }


}

class SingleUserAPIResult extends APIResult {
  User user;

  SingleUserAPIResult(int code, String error) : super();

  SingleUserAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  @override
  String toString() {
    return super.toString() + "user:${user.toString()}";
  }
}

class MultipleUserAPIResult extends APIResult {
  List<User> users;

  MultipleUserAPIResult.fromResponse(APIResult response) {
    this.code = response.code;
    this.error = response.error;
    this.data = response.data;
    this.succeed = response.succeed;
  }

  MultipleUserAPIResult(int code, String error) : super();

  @override
  String toString() {
    return super.toString() + "users:${users.toString()}";
  }
}
