import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:toy_store/Api/api.dart';

class FileUtils
{
  static Future<String> get _localPath async{
    final directory  = await getApplicationDocumentsDirectory();
    return directory.path;
  }
  //general:
  static Future<File> loadFile(String fileName) async{
    final path = await _localPath;
    return File('$path/$fileName');
  }

  static Future<File> loadBase(String base64ImageStr, String fileName)async{
    final  decodedBytes = base64Decode(base64ImageStr);
    File file = await FileUtils.loadFile(fileName);
    await file.writeAsBytes(decodedBytes);
    return file;
  }

  static Future<bool> isFileExists(String fileName) async{
    final file = await loadFile(fileName);
    final exists = await file.exists();
    return exists;
  }
  static Future<dynamic> downloadImage(String FileName, String url) async {
// TODO STORAGE FIX
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/'+FileName);

    if (file.existsSync()) {
      var image = await file.readAsBytes();
      return image;
    } else {
      try{
        var request = await http.get(url.contains("https")?url: ( API.API_URL +url )).timeout(Duration(seconds:10));
        var bytes =  request.bodyBytes;//close();
        await file.writeAsBytes(bytes);
        print("File utils "+file.path);
        return bytes;
      }catch(error){
        return null;
      }
    }
  }
  static Future<void> deleteFile(String fileName) async{
    final file = await loadFile(fileName);
    await file.delete();
  }
  //string files:
  static Future<String> loadFileStr(String fileName) async{
    final file = await loadFile(fileName);
    final str = await file.readAsString();
    return str;
  }
  static Future<String> saveFileStr(String fileName,String contents) async{
    final file = await loadFile(fileName);
    final newFile = await file.writeAsString(contents);
    final str = await newFile.readAsString();
    return str;
  }

}
