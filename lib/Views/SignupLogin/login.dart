
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toy_store/Api/api.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/SignupLogin/signup.dart';
import 'package:toy_store/Views/Utils/custom-circular-button.dart';
import 'package:toy_store/Views/Utils/custom-snack-bar.dart';
import 'package:toy_store/main.dart';

import '../main-navigation.dart';

class Login extends StatefulWidget {

  bool fromInsideOfApp;
  Login({this.fromInsideOfApp=false});
  @override
  _LoginPageState createState() => _LoginPageState(fromInsideOfApp);
}

class _LoginPageState extends State<Login> {

  String _email = "";
  String _password = "";
  bool _totalValidated = false;
  bool _loginSuccess = false;
  bool _isClicked = false;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool _obscureText = true;
  bool _animationStatus = false;
  bool fromInsideOfApp;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  _LoginPageState(this.fromInsideOfApp);
  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: null,
      body: SingleChildScrollView(
          child: Container(
            height: _height,
            width: _width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Padding(
                  padding: EdgeInsets.symmetric(vertical: _height/10),
                  child: Text("ورود به حساب کاربری", style: TextStyle(color: Theme.of(context).textSelectionColor, fontSize: 18, fontWeight: FontWeight.bold),),
                ),
                Expanded(child: Column(),),
                Padding(
                  padding: EdgeInsets.only(top: _height / 15),
                  child: Form(
                    autovalidate: false,
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _inputBox(_height, _width, emailController, "ایمیل"),
                        _inputBox(_height, _width, passwordController, "رمز عبور"),
                        _loginBtn(_height, _width),
                        _forgetPassword(_height, _width),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }

  Widget _forgetPassword(double _height, double _width){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Padding(
          padding:  EdgeInsets.only(bottom: _height/10),
          child: Text("فراموشی رمز عبور", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),),
        ),
        GestureDetector(
          onTap: (){
            MyApp.pushPage(route: Signup(), context: context);
          },
          child: Padding(
            padding:  EdgeInsets.only(bottom: _height/10),
            child: Text("حساب کاربری ندارم", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),),
          ),
        ),
      ],
    );
  }
  Widget _loginBtn(double _height, double _width){

    return Padding(
      child: GestureDetector(
        onTap: () {
          setState(() {
            _isClicked = true;
          });
          Future.delayed(const Duration(milliseconds: 100),
                  () {
                    setState(() {
                      _isClicked = false;
                    });
              });
          if (_formKey.currentState.validate()) {
            _login();//todo


//              Future.delayed(const Duration(milliseconds: 2000),
//                      () {
//                fromInsideOfApp?MyApp.popPage(context: context):Navigator.of(context).pushReplacement(MaterialPageRoute(
//                      builder: (context) => MainNavigation(),
//                    ));
//                  });
//            });
          }

          //    _controls.play("Stop");
        },
        onTapCancel: () {
          setState(() {
            _isClicked = false;
          });
        },
        onTapDown: (_) {
          setState(() {
            _isClicked = true;
          });
        },
        onTapUp: (_) {
          setState(() {
            _isClicked = false;
          });
        },
        child: CustomCircularButton(
          "ورود",

          _totalValidated
              ?_isClicked?Colors.transparent: Theme.of(context).accentColor
              : _isClicked?Colors.transparent:Theme.of(context).accentColor.withOpacity(0.5),
          _isClicked? Theme.of(context).accentColor:Theme.of(context).primaryColorLight,
          Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
          width: _width / 7 * 5,
          height: _height / 14,
          elevation: 0,
          textSize: 18,
          radius: 5,
          bordersize: 2,
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: _height/20),
    );

  }

  Widget _inputBox(double _height, double _width,
      TextEditingController controller, String lable) {
    return Container(
        width: _width * 7 / 8,
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: _height / 40),
            child: TextFormField(
              onTap: () {},


              obscureText: lable == "ایمیل" ? false : (_obscureText?true:false),
              controller: controller,
              cursorColor: Theme.of(context).accentColor,
              decoration: InputDecoration(
                prefixIcon: lable == "ایمیل"
                    ? Icon(
                  Icons.email,
                )
                    : Icon(
                  Icons.remove_red_eye,
                ),
                contentPadding: EdgeInsets.all(15.0),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Theme.of(context).accentColor,
                  ),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Colors.red,
                  ),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Colors.red,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 2,
                    color: Theme.of(context).cardColor,
                  ),
                ),

                labelText: lable,
                labelStyle: TextStyle(
                    color: Theme.of(context).accentColor, fontSize: 12),
              ),
              keyboardType: lable == "ایمیل"
                  ? TextInputType.emailAddress
                  : TextInputType.visiblePassword,
              validator: lable == "ایمیل" ? validateEmail : validatePassword,
              onChanged: (String val) {
                if (lable == "رمز عبور")
                  _password = val;
                else if (lable == "ایمیل") _email = val;

                setState(() {
                  _totalValidation();
                });
              },
              autovalidate: false,
            ),
          )
        ]));
  }

  _totalValidation() {
    if (_email.length > 0 && _password.length > 0) {
      setState(() {
        _totalValidated = true;
      });
    } else {
      setState(() {
        _totalValidated = false;
      });
    }
  }

  String validateEmail(String value) {
    if (!value.contains("@")) {
      return 'ایمیل اشتباه وارد شده';
    } else if (value == "") {
      return 'هیچ مقداری وارد نشده ';
    } else {
      return null;
    }
  }

  String validatePassword(String value) {
    if (value.length < 6)
      return 'رمذ عبور باید حداقل 6 حرف باشد';
    else
      return null;
  }
  _login()async {
    var body = {
      "email": _email,
      "password": _password
    };
    var result = await API.userAPI.login(body);
    if (result.isSuccess()) {
      print(result.user);
      AppState.instance.setCurrentUser(result.user);
      _formKey.currentState.save();
      SystemChannels.textInput
          .invokeMethod('TextInput.hide')
          .then((_) {});
      showFloatingFlushbar(context,
          msg: StringUtils.errorStringHandler(
              "ورود به حساب کاربری با موفقیت انجام شد"),
          isDismissible: true,
          baseColor: Colors.green,
          icon: Icons.check);
      Future.delayed(const Duration(milliseconds: 1000), () {
        MyApp.pushPage(
            context: context, route: MainNavigation());
      });
    } else {
      showFloatingFlushbar(context,
          msg: StringUtils.errorStringHandler(result.error),
          isDismissible: true,
          baseColor: Colors.red,
          icon: Icons.error);
      Future.delayed(const Duration(milliseconds: 1000), () {

      });
    }
  }
}
