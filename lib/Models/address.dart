class Address{
  String city;
  String location;
  String postalCode;//max_length=16 =>
  String receiverName;//max_length=50 =>
  String receiverLastName;//max_length=50 =>
  String receiverPhone;//max_length=11 => receiver_phone



  Address.fromJson(dynamic js){

    this.city = js['city'];
    this.location = js['location'];
    this.postalCode = js['postal_code'];
    this.receiverName = js['receiver_name'];
    this.receiverLastName = js['receiver_last_name'];
    this.receiverPhone = js['receiver_phone'];

  }

  dynamic toJson(){

    return {
      'city' : city,
      'location' : location,
      'postal_code' : postalCode,
      'receiver_name': receiverName,
      'receiver_last_name': receiverLastName,
      'receiver_phone': receiverPhone,
    };

  }
  @override
  String toString() {
    return super.toString()+toJson().toString();
  }
}