class StringUtils{
  static final String NEWEST_TXT = "جدید ترین ها";
  static final String BEST_SELLER = "پرفروش ترین ها";
  static final String MOST_VIEWER = "پربازدید ترین ها";
  static final String NO_PRODUCTS_FOUND = "موردی یافت نشد!";
  static final String RETURN_TXT = "بازگشت";
  static final String API_TOKEN = "biiiiuivapisuiiu";
  static final String JSON_CONTENT_TYPE = "application/json";
  static final String API_URL = "http://192.168.1.5:5000/api/v1/";
  static final String API_URL_BASE = "http://192.168.1.3:5000";
  static int TIME_OUT_DURATION = 25;
  static final String INTERNET_ERROR = "عدم اتصال به اینترنت";


  static void largeLog(String content) {
    if (content.length > 4000) {
      print("Nazanin Large Log " + content.substring(0, 4000));
      print("Nazanin Large Log " + content.substring(4000));
    } else
      print(content);
  }

  static String errorStringHandler(String error)
  {
    print("error in string utils");
    print(error);
    if(error==""||error==null)
      error="Unknown";
    else if(error.contains('empty field'))
      error="فیلد ها خالی هستند";
    else if(error.contains('wrong password'))
      error="پسورد اشتباه است";
    else if(error.contains('User Not Found'))
      error="کاربر پیدا نشد";
    else if(error.contains('invalid user token'))
      error="توکن نامعتبر";
    else if(error.contains('object not found'))
      error="آبجکت پیدا نشد";
    else if(error.contains('ticket not found'))
      error="تیکت پیدا نشد";
    else if(error.contains('status not found'))
      error="استاتوس پیدا نشد";
    else if(error.contains('project not found'))
      error="پروژه پیدا نشد";
    else if(error.contains('priority not found'))
      error="الویت پیدا نشد";
    else if(error.contains('permission not found'))
      error="دسترسی پیدا نشد";
    else if(error.contains('job not found'))
      error="دستور پیدا نشد";
    else if(error.contains('company not found'))
      error="شرکت پیدا نشد";
    else if(error.contains('category not found'))
      error="کاتاگوری پیدا نشد";
    else if(error.contains('answer not found'))
      error="جواب پیدا نشد";
    else if(error.contains('user')&&error.contains('permission not found'))
      error="سطح دسترسی به کاربر پیدا نشد";
    else if(error.contains('Email must be Unique'))
      error="این ایمیل وجود دارد";
    else if(error.contains('Mobile must be Unique'))
      error="این شماره وجود دارد";
    else if(error.contains('Mobile size must be eleven'))
      error="شماره موبایل باید 11 رقم باشد";
    else if(error.contains("Invalid email"))
      error="ایمیل نامعتبر است";
    else if(error.contains("already assign to same person"))
      error=" شخص اختصاص داده شده";
    else if(error.contains("Title should be more than Three character"))
      error="عنوان باید بیشتر از سه حرف باشد";
    else if(error.contains("Content should be more than Three character"))
      error="شرح کامل تیکت باید بیشتر از سه حرف باشد";
    else if(error.contains("Connection failed")|| error.contains("TimeoutException"))
      error=INTERNET_ERROR;
    else if(error.contains("password key not found"))
      error="کد ارسال شده نا معتبر است";
    else if(error.contains("SocketException")||error.contains("Connection refused"))
      error="دسترسی به سرور نداریم";
    else if(error.contains("Bad Request")){
      error = "خطا";
    }
    return error;
  }

}