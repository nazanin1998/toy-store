import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Views/Product/product-horizontal-card.dart';

class ProductHorizontalList extends StatelessWidget {

  List<Product> products;
  ProductHorizontalList(this.products);
  @override
  Widget build(BuildContext context) {
    double width =  MediaQuery.of(context).size.width;
    double height =  MediaQuery.of(context).size.height;

    return Container(
      width: width,
      height: height/3,
      color: Colors.transparent,
      child: ListView.builder
        (
          padding: EdgeInsets.only(left: 10, right: 10),
          itemCount: products.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext ctxt, int index) {
            return ProductHorizontalCard(products[index]);
          }
      ),
    );

  }
}
