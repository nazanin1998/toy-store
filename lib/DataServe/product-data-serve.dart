import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:toy_store/Api/api.dart';
import 'package:toy_store/Api/app-state.dart';
import 'package:toy_store/Models/product.dart';
import 'package:toy_store/Utills/string-utills.dart';
import 'package:toy_store/Views/Product/product-grid-list.dart';
import 'package:toy_store/Views/Utils/custom-error-widget.dart';
import 'package:toy_store/Views/Product/product-horizontal-list.dart';

class ProductDataServer extends StatefulWidget {
  String params;
  bool isVertical;

  ProductDataServer({this.params="", this.isVertical=true});
  @override
  _ProductDataServerState createState() => _ProductDataServerState(params, isVertical);
}

class _ProductDataServerState extends State<ProductDataServer> {
  String _params;
  String error = "";

  bool isLoadingState = true;
  bool isVertical;

  List<Product> products = [];

  RefreshController _refreshController = RefreshController(initialRefresh: false);

  _ProductDataServerState(this._params, this.isVertical){
      getProducts();
  }

  @override
  Widget build(BuildContext context) {

    double height = MediaQuery.of(context).size.height;

    if(error != "") {
      return isVertical?Scaffold(
        body: Center(child:CustomErrorWidget(error: StringUtils.errorStringHandler(error)))
      ):CustomErrorWidget(error: StringUtils.errorStringHandler(error),
          callBack: () => getProducts());
    }else{
      if(isLoadingState){
        return isVertical?Scaffold(
          body: Center(child: CircularProgressIndicator(),),
        ):Container(height:(height/4),child: Center(child: CircularProgressIndicator()));
      }else if(products.length == 0) {

        return isVertical?Scaffold(
          body:Center(child: CustomErrorWidget(
            error: StringUtils.NO_PRODUCTS_FOUND, callBack: () => getProducts(),)) ,
        ):Container(height: (height/4),
            child: Center(child: CustomErrorWidget(
              error: StringUtils.NO_PRODUCTS_FOUND, callBack: () => getProducts(),)));
      }else{

        return isVertical?
         Expanded(
          child: SmartRefresher(
              enablePullDown: true,
              enablePullUp: true,
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: ProductGridList(products)
          )):ProductHorizontalList(products);
      }
    }
  }



  getProducts()async{
      var result = await API.productAPI.getAllProducts(_params);
      if(result.isSuccess()){
        setState(() {
          AppState.instance.addProducts(result.products);
          products = result.products;
          isLoadingState = false;
        });
      }else{
        setState(() {
          isLoadingState = false;
          error = result.error;

        });
      }

  }

//  addProducts(List<Product> items){
//    if(products!=null) {
//      print("OLD APP_STATE PRODUCTS LENGTH IS " + products.length.toString());
//    }else{
//      products = [];
//    }
//    if(items.length == 0){
//      return;
//    }else{
//      for(var i = 0 ; i < items.length ; i++){
//        bool hasItem =false;
//        for(var j = 0 ; j < products.length ; j++){
//          if(products[j].id == items[i].id){
//            hasItem = true;
//          }
//        }
//        if(!hasItem)
//          products.add(items[i]);
//      }
//      print("ADD NEW PRODUCTS");
//      print("NEW APP_STATE PRODUCTS LENGTH IS "+ products.length.toString());
//    }
//  }
}
