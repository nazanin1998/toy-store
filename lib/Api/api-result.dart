class APIResult {
  int code;
  String error;
  bool succeed;
  dynamic data;

  APIResult({this.code, this.succeed, this.data, this.error});

  bool noInternet() {
    return code == -1;
  }

  bool isSuccess() {
    return succeed;
  }

  bool isFailed() {
    return !succeed;
  }

  String toString(){
    return "\ncode:$code\nerror:$error\ndata:$data\n";
  }
}
